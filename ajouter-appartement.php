<?php
	include_once 'header.php';
	include_once 'includes/dbh-conn.php';



// if (isset( $_GET['IdMaison'] ) &&  isset( $_SESSION['uId'] ) ) {
 	echo "<a href='gerer-maison-perso.php' class = 'retour_maison'>Mes maisons</a>  <a href='gerer-maison-perso.php?IdMaison=".$_GET['IdMaison']."' class='retour_appart'> Mes appartements </a>";
// }

// else {
// 	ob_end_clean();
// 	header('HTTP/1.0 404 Not Found');
// 	exit();
// }


    

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="stylesheets/style_ajout_appart.css" rel="stylesheet">
    <title>Document</title>
 </head>
 <body>

    <form action=<?php echo "addappart.php?IdMaison=".$_GET['IdMaison'];?> method="POST" class="ajouter-appart-form">


        <div class="ajouter-appart-form">
            <label for="nb_hab">Nombre habitants : </label>
            <input type="text" name="nb_hab" id="nb_hab" placeholder="Nombre">
        </div>
        <div class="ajouter-appart-form">
            <label for="degre_secu">Degré de Sécurité : </label>
            <input type="text" name="degre_secu" id="degre_secu" placeholder="Sécurité">
        </div>

        <div class="ajouter-appart-form">
            <label for="numero_appart">Numéro d'appartement : </label>
            <input type="text" name="numero_appart" id="numero_appart" placeholder="Numéro Appartement">
        </div>

        <div class="ajouter-appart-form">
            <label for="type_app">Type appartement : </label>
            <select id="type_app" name="type_app">
                <option value="Studio">Studio</option>
                <option value="T1">T1</option>
                <option value="T2">T2</option>
                <option value="T3">T3</option>
                <option value="T3 T4">T3 T4</option>
                <option value="T5">T5</option>
                <option value="T6">T6</option>
                <option value="Duplex">Duplex</option>
                <option value="Triplex">Triplex</option>
                <option value="Souplex">Souplex</option>
                <option value="Loft">Loft</option>
            </select>
        </div>

        </div>
        <div class="ajouter-appart-form">
        <button type="submit" name="submit"> Ajouter </button>

        </div>
    </form>
 </body>
</html>