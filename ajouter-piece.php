<?php
	include_once 'header.php';
	include_once 'includes/dbh-conn.php';



if (isset( $_GET['IdAppart'] ) && isset( $_SESSION['uId'] ) ) {

    // Trouver la maison d'origine

    $sql = "SELECT * 
			FROM proprietaire P
			WHERE (P.IdMaison = (SELECT A.IdMaison
                                FROM appartement A
                                WHERE A.IdAprt = ?)) 
                    AND (P.IdU = ?)";

	$stmt = mysqli_stmt_init($conn);
	mysqli_stmt_prepare($stmt, $sql);
	mysqli_stmt_bind_param($stmt, "ii", $_GET['IdAppart'] , $_SESSION['uId']);
	mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);
    $idMaison = mysqli_fetch_assoc($result)['IdMaison'];

	echo "<a href='gerer-maison-perso.php'>Mes maisons</a>  
          <a href='gerer-maison-perso.php?IdMaison=".$idMaison."'> Mes appartements </a>
          <a href='MesPieces.php?&IdAppart=".$_GET['IdAppart']."'> Mes pieces</a> ";
}

else {
	ob_end_clean();
	header('HTTP/1.0 404 Not Found');
	exit();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="addpiece.php?IdAppart=<?php echo $_GET['IdAppart']?>" method="POST" class="ajouter_piece_form">

        <label for="type_piece">Nom de la piece</label>
        <input type="text" name="Nom_piece" id="Nom_piece" placeholder="Nom">

        <label for="type_piece">Type piece</label>
        <select id="type_piece" name="type_piece">
                <option value="1">Entrée</option>
                <option value="2">Cuisine</option>
                <option value="3">Séjour</option>
                <option value="4">Chambre</option>
                <option value="5">Piece d'eau</option>
                <option value="6">Cellier</option>
        </select>
        
        <button type="submit" name="submit"> ajouter </button>

    </form>
</body>
</html>