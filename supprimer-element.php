<?php

include_once 'includes/dbh-conn.php';

if (isset($_POST['delete_button'])){

    switch ($_GET['element']) {


        case 'Maison':

            $sql = "DELETE FROM maison
                    WHERE IdMaison = ?";

            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt, $sql);
            mysqli_stmt_bind_param($stmt, "i", $_GET['IdMaison']);
            mysqli_stmt_execute($stmt);

            break;

        case 'appartement':

            $sql = "DELETE FROM appartement
                    WHERE IdAprt = ?";

            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt, $sql);
            mysqli_stmt_bind_param($stmt, "i", $_GET['IdAprt']);
            mysqli_stmt_execute($stmt);

            break;

        
        case 'piece':

            $sql = "DELETE FROM piece
                    WHERE IdPiece = ?";

            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt, $sql);
            mysqli_stmt_bind_param($stmt, "i", $_GET['IdPiece']);
            mysqli_stmt_execute($stmt);

            break;



        case 'appareil':

            $sql = "DELETE FROM appareil
                    WHERE IdApp = ?";

            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt, $sql);
            mysqli_stmt_bind_param($stmt, "i", $_GET['IdAppareil']);
            mysqli_stmt_execute($stmt);

            break;

        case 'user':

            $sql = "DELETE FROM utilisateur
                    WHERE IdU = ?";

            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt, $sql);
            mysqli_stmt_bind_param($stmt, "i", $_GET['IdU']);
            mysqli_stmt_execute($stmt);
            session_start();
            session_unset();
            session_destroy();
            header('Location: ./index.php');
            break;

        default:
            header('Location: ./profil.php');


    }

    header('Location: ./index.php?delete=successful');
    exit();



}

else{

    header('Location: ./profil.php');
    exit();



}






?>