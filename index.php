<?php
	include_once 'includes/dbh-conn.php';
	include_once 'header.php';
?>


<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" lang="fr">
		<meta name="viewport" content="width=device-width, initial-scale='1.0'">
		<link href="stylesheets/style.css" rel=stylesheet>
		<title>Maison Econome</title>

	</head>
	<body>

			<div class="contenu">

				<h2>Simulation de maisons économes</h2>
				<p> Ce site a pour but de simuler l'utilisation de ressources et les emissions de différentes substances
					des appareils ménagers.
					Si vous avez plus de 18 ans, vous pouvez créer votre compte et vous connecter via votre Nom de famille ou votre Email.</br>
					Vous pouvez créer des maisons, y ajouter des appartements, dans lesquels on peut ajouter différentes pièces.</br>
					Enfin, on peut ajouter, dans chacun de ces pièces, des appareils ménagers parmis une treintaine de types.</br>
					Renseignez les consommation de ces appareils, et vous pouvez commencer à les allumer.</br>
					Il est possible d'allumer plusieurs appareils en meme temps, plusieurs fois le meme dans la meme journée, et le 
					temps de consommation est calculé automatiquement.</br></br>
					Vous pouvez également commencer une ou plusieurs location depuis votre profil.
					Il suffit de renseigner les différents champs qui correspondent à un appartement d'un propriétaire.</br>
					Celui-ci aura une notification lors de sa prochaine connexion, et pourra accepter ou refuser la demande.<p>
				

	</body>
</html>
