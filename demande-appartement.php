<?php
include_once 'includes/dbh-conn.php';
include_once 'header.php';


if (isset($_POST['submit'])){

    // Récuperer les infos du form et les sécuriser

    //---------------------------------------------------//

    $idLocataire = $_SESSION['uId'];
    $nom_proprio = $_POST['nom_proprio'];
    $prenom_proprio = $_POST['prenom_proprio'];
    $type_appartement = $_POST['type_appartement'];
    $numero_appart = $_POST['numero_appart'];
    $Rue = $_POST['Rue'];
    $Ville_maison = $_POST['Ville_maison'];
    $date_location_debut = $_POST['date_location_debut'];

    //---------------------------------------------------//

    $nom_proprio = mysqli_real_escape_string($conn,$nom_proprio);
    $prenom_proprio = mysqli_real_escape_string($conn, $prenom_proprio);
    $type_appartement = mysqli_real_escape_string($conn, $type_appartement);
    $numero_appart = mysqli_real_escape_string($conn, $numero_appart);
    $Rue = mysqli_real_escape_string($conn, $Rue);
    $Ville_maison = mysqli_real_escape_string($conn, $Ville_maison);
    $date_location_debut = mysqli_real_escape_string($conn, $date_location_debut);

    //---------------------------------------------------//


    // On vérifie que la maison demandée existe, puis on ajoute dans la table demande_location.



    $sql = "SELECT A.IdAprt
            FROM maison M 
            INNER JOIN appartement A ON (A.IdMaison = M.IdMaison)
            INNER JOIN proprietaire P ON (A.IdMaison = P.IdMaison)

            WHERE (P.IdU = (SELECT IdU 
                            FROM utilisateur
                            WHERE Nom=? AND Prenom=?)) 
            AND (M.Rue = ?) 
            AND (M.Ville = ?)
            AND A.IdTypeAppartement = (SELECT IdTypeAppartement
                                       FROM typeapp
                                       WHERE LibelleTypeApp = ?)
            AND (A.NumeroApprt = ?)
    
            ";


    $stmt = mysqli_stmt_init($conn);
    mysqli_stmt_prepare($stmt, $sql);
    mysqli_stmt_bind_param($stmt, "sssssi", $nom_proprio, $prenom_proprio, 
                                            $Rue, $Ville_maison, 
                                            $type_appartement, $numero_appart);
    mysqli_stmt_execute($stmt);

    $res = mysqli_stmt_get_result($stmt);


    // On trouve différents appartements qui répondent à la demande.
    // On vérifie ensuite que ces appartements ne sont pas déja loués.

    if (mysqli_num_rows($res)!=0)  {


        $idApprt = mysqli_fetch_object($res);

        $sql2 = "INSERT INTO demande_location(IdProprio, IdAprt,IdLocataire)
                VALUES ((SELECT IdU 
                            FROM utilisateur
                            WHERE Nom=? AND Prenom=?),
                        ?,
                        ?);";


        $stmt2 = mysqli_stmt_init($conn);
        mysqli_stmt_prepare($stmt2, $sql2);
        mysqli_stmt_bind_param($stmt2, "ssis", $nom_proprio, $prenom_proprio, $idApprt, $idLocataire);

        if ($res2 = mysqli_stmt_execute($stmt2)){

            
            echo "Demande envoyée !";

            mysqli_stmt_close($stmt);
            mysqli_close($conn);

            header('Location: ./louer-un-appart.php?succes');

            exit();
        
        } else {

            header('Location: ./louer-un-appart.php?erreur=MaisonInexistante');
            exit();

        }
    }




} else {

    header('Location: ./louer-un-appart.php.php?button=erreur');
	exit();


}



?>