<?php
	include_once 'includes/dbh-conn.php';
	include_once 'header.php';
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="stylesheets/style_gerer_maison.css" rel=stylesheet>
    <title>Mes Maisons</title>
</head>
    <body>

		<?php

		// Si IdMaison est dans l'url, on veut s'interesser à une maison 

		if (isset( $_GET['IdMaison'] ) ) {


			//Ne regarder que ses propres maisons
			// On fait une requete SQL dans la table proprio et on regarde le nombre de resultats


			$check_id = "SELECT *
						FROM proprietaire P
						WHERE (P.IdMaison = ?) AND (P.IdU = ?); ";

			$stmt_check = mysqli_stmt_init($conn);
			mysqli_stmt_prepare($stmt_check, $check_id);
			mysqli_stmt_bind_param($stmt_check, "ii", $_GET['IdMaison'], $_SESSION['uId']);
			mysqli_stmt_execute($stmt_check);

			$result = mysqli_stmt_get_result($stmt_check);
			$nb = mysqli_fetch_array($result); // Null si aucun résultat

			if (!$nb) { 
				ob_end_clean();
				header('HTTP/1.0 404 Not Found');
				exit;
			}

			// L'IdMaison dans l'url appartient bien à l'utilisateur connecté 
			// 
			// On sélectionne la bonne maison
			

			echo "<a href='gerer-maison-perso.php'>Mes maisons</a>";
			echo "<h1>Mes appartements</h1>";


			$sql = "SELECT * 
					FROM maison M
					WHERE (M.IdMaison = ?)";

			$stmt = mysqli_stmt_init($conn);
			mysqli_stmt_prepare($stmt, $sql);
			mysqli_stmt_bind_param($stmt, "i", $_GET['IdMaison']);
			mysqli_stmt_execute($stmt);

			$result = mysqli_stmt_get_result($stmt);


			if ($result)
			{

				$link_add_app = "ajouter-appartement.php?IdMaison=".$_GET['IdMaison'];

				echo "<fieldset class='check-cont'>
						<div class='button-add_appart'>
							<a href='$link_add_app'> Ajouter un appartement</a>
						</div>
				
					</fieldset>";


				// On affiche le nombre d'appart, et un lien vers chacuns

				$get_apparts = "SELECT * 
								FROM appartement A
								WHERE (A.IdMaison = ?)";

				$stmt_app = mysqli_stmt_init($conn);
				mysqli_stmt_prepare($stmt_app, $get_apparts);
				mysqli_stmt_bind_param($stmt_app, "i", $_GET['IdMaison']);
				mysqli_stmt_execute($stmt_app);

				$result_app = mysqli_stmt_get_result($stmt_app);
				$i = 0;

				while ($colonne = mysqli_fetch_assoc($result_app) ){

					$IdAppart = $colonne['IdAprt'];

					$trouver_locataires = "SELECT U.Nom, U.Prenom
											FROM `location` L
											INNER JOIN utilisateur U ON (L.IdU = U.IdU)
											WHERE L.IdAprt = ?";

					$stmt_loc = mysqli_stmt_init($conn);
					mysqli_stmt_prepare($stmt_loc, $trouver_locataires);
					mysqli_stmt_bind_param($stmt_loc, "i", $IdAppart);
					mysqli_stmt_execute($stmt_loc);

					$result_loc = mysqli_stmt_get_result($stmt_loc);
					$locataire = mysqli_fetch_assoc($result_loc);

					$i += 1;
					$link_pieces = "MesPieces.php?IdAppart=".$colonne['IdAprt'];

					if ($locataire){

						echo "<fieldset class='check-cont'>
						Appartement ".$i.": Loué par ".$locataire['Nom']." ".$locataire['Prenom']."  
						<a href=".$link_pieces."> Voir mes pièces </a>
						<form action='supprimer-element.php?element=appartement&IdAprt=".$colonne['IdAprt']."' method='POST'>
									<button type='submit' name='delete_button'> Supprimer cet appartement</button>
							</form>

						</fieldset>";



					}else {
						
						echo "<fieldset class='check-cont'>
						Appartement ".$i."       Numéro : ".$colonne['NumeroApprt']."
							<a href=".$link_pieces."> Voir mes pièces </a>
							<form action='supprimer-element.php?element=appartement&IdAprt=".$colonne['IdAprt']."' method='POST'>
									<button type='submit' name='delete_button'> Supprimer cet appartement</button>
							</form>
						
						</fieldset>";
					}

				}

			} else {

				echo "Erreur";
				exit();
			}

			mysqli_stmt_close($stmt);
			mysqli_close($conn);
		}

		?>

		

		<div class="cat-container">
			<?php

				// L'utilisateur visite la page globale (pas dans une maison en particulier)

				if (empty( $_GET['IdMaison'] ) ) {

					echo "<h1>Mes maisons </h1>";


					$sql = "SELECT * 
							FROM maison M, proprietaire P
							WHERE (M.IdMaison = P.IdMaison) AND (P.IdU = ?) ";

					$stmt = mysqli_stmt_init($conn);
					mysqli_stmt_prepare($stmt, $sql);
					mysqli_stmt_bind_param($stmt, "i", $_SESSION['uId']);
					mysqli_stmt_execute($stmt);
					

					$result = mysqli_stmt_get_result($stmt);


					while ($colonne = mysqli_fetch_assoc($result) ){

						$link_maison = "gerer-maison-perso.php?IdMaison=".$colonne['IdMaison'];


						$sql_nb_appart = "SELECT COUNT(*) as total
										FROM appartement A
										WHERE (A.IdMaison = ?)";

						$stmt_nb_app = mysqli_stmt_init($conn);
						mysqli_stmt_prepare($stmt_nb_app, $sql_nb_appart);
						mysqli_stmt_bind_param($stmt_nb_app, "i", $colonne['IdMaison']);
						mysqli_stmt_execute($stmt_nb_app);

						$result_nb_app = mysqli_stmt_get_result($stmt_nb_app);

						$nb_appartements = mysqli_fetch_assoc($result_nb_app)['total'];

						echo "<fieldset class='check-cont'>							
							<div class='maison-container'>
								<p>	<span class='maisons-box'>" .$colonne['Nom']. "</span> (".$colonne['Numero']." rue \"".$colonne['Rue']."\" à ".$colonne['Ville'].")</br></br>
									<span class='maisons-box'>Nombre d'appartements : " . $nb_appartements . "</span> </br></br>
								<a class='click_maison' href='$link_maison'> Acceder </a>
								<form action='supprimer-element.php?element=Maison&IdMaison=".$colonne['IdMaison']."' method='POST'>
									<button type='submit' name='delete_button'> Supprimer cette maison</button>
								</form>
							</div>

						</fieldset>";
					}

					mysqli_stmt_close($stmt);
					mysqli_close($conn);
				}

				?>


			</form>
		</div>


	</body>
</html>