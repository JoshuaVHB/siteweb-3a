<?php
	include_once 'includes/dbh-conn.php';
	include_once 'header.php';


    // Check si l'appart appartient bien à l'utilisateur

    $sql = "SELECT * 
			FROM proprietaire P
			WHERE (P.IdMaison = (SELECT A.IdMaison
                                FROM appartement A
                                WHERE A.IdAprt = ?)) 
                    AND (P.IdU = ?)";

	$stmt = mysqli_stmt_init($conn);
	mysqli_stmt_prepare($stmt, $sql);
	mysqli_stmt_bind_param($stmt, "ii", $_GET['IdAppart'] , $_SESSION['uId']);
	mysqli_stmt_execute($stmt);

	$result = mysqli_stmt_get_result($stmt);

    // On trouve que l'appart est bien à l'utilisateur
	if (mysqli_fetch_assoc($result))
	{ 

        $sql_maison_id = "SELECT *
                        FROM appartement A
                        WHERE A.IdAprt = ?";

        $stmt_maison_id = mysqli_stmt_init($conn);
        mysqli_stmt_prepare($stmt_maison_id, $sql_maison_id);
        mysqli_stmt_bind_param($stmt_maison_id, "i", $_GET['IdAppart']);
        mysqli_stmt_execute($stmt_maison_id);

        $result = mysqli_stmt_get_result($stmt_maison_id);



        $maison_id = mysqli_fetch_assoc($result)['IdMaison'];
        

        echo "<a href='gerer-maison-perso.php'>Mes maisons</a>  
            <a href='gerer-maison-perso.php?IdMaison=".$maison_id."'> Mes appartements </a><br>";

        echo "<h1>Mes Pieces</h1>";
        echo "<a href='ajouter-piece.php?IdAppart=".$_GET['IdAppart']."'> Ajouter une pièce</a> ";


        $sql = 
        "SELECT P.NomPiece, T.LibelleTypePiece, P.IdPiece
        FROM piece P INNER JOIN appartenirpiece A ON (P.IdPiece = A.IdPiece) INNER JOIN typepiece T ON (T.IdTypePiece = A.IdTypePiece)
        WHERE (P.IdAprt = ?); "; //Tema la requete

        $stmt = mysqli_stmt_init($conn);
        mysqli_stmt_prepare($stmt, $sql);
        mysqli_stmt_bind_param($stmt, "i", $_GET['IdAppart']);
        mysqli_stmt_execute($stmt);

        $result = mysqli_stmt_get_result($stmt);

        while ($colonne = mysqli_fetch_assoc($result) ){

            echo "<p>Ma piece qui s'apelle : ".$colonne['NomPiece']."</p>
                  <p>C'est un(e) : ".$colonne['LibelleTypePiece']."</p>
                  <a href='ajouter-appareil.php?IdPiece=".$colonne['IdPiece']."'> Voir / ajouter des appareils dans cette piece</a>
                  <form action='supprimer-element.php?element=piece&IdPiece=".$colonne['IdPiece']."' method='POST'>
        
                    <button type='submit' name='delete_button'>Supprimer cette piece</button>

                </form> 
                  ";


        }


    }
    else 
    {
        echo "degage";
    }

?>