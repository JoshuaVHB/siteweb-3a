<?php
	include_once 'includes/dbh-conn.php';
	include_once 'header.php';
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profil</title>
    <link href="stylesheets/style_profil.css" rel=stylesheet>
</head>
<body>
    <h1>Bonjour <?php echo $_SESSION['Prenom'];?> !</h1>
    <a href="ajouter-maison.php" class='bouton'>Ajouter une maison</a></br>
    <a href="gerer-maison-perso.php" class='bouton'>Gerer mes maisons</a></br>
    <a href="consommation-perso.php" class='bouton'>Ma conso</a></br>

    <?php 

    if (isset($_SESSION['uId'])){


        // Check si l'utilisateur loue déjà une maison


        $sql = "SELECT *
                FROM `location` L 
                WHERE IdU = ? AND DateFin IS NULL";
        
        $stmt = mysqli_stmt_init($conn);
        mysqli_stmt_prepare($stmt, $sql);
        mysqli_stmt_bind_param($stmt, 'i', $_SESSION['uId']) ;
        mysqli_stmt_execute($stmt);

        $res = mysqli_stmt_get_result($stmt);

        $location = mysqli_fetch_assoc($res);

        if ($location){

            echo "<a href=ma-location.php?IdAprt=".$location['IdAprt']." class='bouton'>Voir ma location</a></br>";


        } 
            
        echo "<a href='louer-un-appart.php' class='bouton'>Louer un appartement </a></br>
              <a href='modifier-profil.php' class='bouton'> Modifier mon profil </a>";

        
    }
    ?>

    <img src="stylesheets/cozy.svg" class="test">



    
</body>
</html>