<?php
	include_once 'includes/dbh-conn.php';
	include_once 'header.php';
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" lang="fr">
		<link href="stylesheets/styleFORM.css" rel=stylesheet>
		<meta name="viewport" content="width=device-width, initial-scale='1.0'">
		<title>Inscription</title>
	</head>

	<body>
		<form action="includes/sign.php" method="post" class="form">
			<fieldset>

				<?php

					if (!isset($_GET['erreur'])) {

						echo '<fieldset class="formulaire">
								<legend>Créez votre compte</legend>
								<div class="div_colonne" type="gauche">
								<input type="password" name="pwd" placeholder="Mot de passe"/><br><br>
								<input type="password" name="repeatpwd" placeholder="Répetez votre mot de passe"/><br><br>
								<input type="text" name="ndf" placeholder="Nom"/><br><br>
								<input type="text" name="prenom" placeholder="Prénom"/><br><br>
								</div>
								
								<div class="div_colonne" type="droite">
								<input type="text" name="email" placeholder="Adresse E-mail"/><br><br>
								<div class="radio_genre">
										<label for="Homme">Homme</label>
										<input type="radio" id="Homme" name="Genre" value="Homme" checked/><br><br>
										<label for="Femme">Femme</label>
										<input type="radio" id="Femme" name="Genre" value="Femme" /><br><br>
										<label for="Autre">Autre</label>
										<input type="radio" id="Autre" name="Genre" value="Autre" /><br><br>
								</div>
										<input type="text" name="Birthday" placeholder="Jour de naissance"/><br><br>
										<input type="text" name="tel" placeholder="Numero de téléphone"/><br><br>
								</div>
									<input type="submit" name="envoi" />
							</fieldset>';

						exit();
					}


				
				if (isset($_GET['username'])) {
							$temp_username = $_GET['username'] ;
					}

				if (isset($_GET['mail'])) {
							$temp_mail = $_GET['mail'];
					}




				if (isset($_GET['erreur'])) {

					$erreur = $_GET['erreur'] ;

					if ($erreur == 'champsvides') {

						echo '<fieldset class="formulaire">
								<legend>Créez votre compte</legend>
								<div class="div_colonne" type="gauche">
								<input type="password" name="pwd" placeholder="Mot de passe"/><br><br>
								<input type="password" name="repeatpwd" placeholder="Répetez votre mot de passe"/><br><br>
								<input type="text" name="ndf" placeholder="Nom"/><br><br>
								<input type="text" name="prenom" placeholder="Prénom"/><br><br>
								</div>
								
								<div class="div_colonne" type="droite">
								<input type="text" name="email" placeholder="Adresse E-mail"/><br><br>
								<div class="radio_genre">
										<label for="Homme">Homme</label>
										<input type="radio" id="Homme" name="Genre" value="Homme" checked/><br><br>
										<label for="Femme">Femme</label>
										<input type="radio" id="Femme" name="Genre" value="Femme" /><br><br>
										<label for="Autre">Autre</label>
										<input type="radio" id="Autre" name="Genre" value="Autre" /><br><br>
								</div>
										<input type="text" name="Birthday" placeholder="Jour de naissance"/><br><br>
										<input type="text" name="tel" placeholder="Numero de téléphone"/><br><br>
								</div>
									<input type="submit" name="envoi" />
							</fieldset>';
						exit();


									
					} else if ($erreur == 'invalidmail') {

						echo '<fieldset class="formulaire">
								<legend>Créez votre compte</legend>
								<div class="div_colonne" type="gauche">
								<input type="password" name="pwd" placeholder="Mot de passe"/><br><br>
								<input type="password" name="repeatpwd" placeholder="Répetez votre mot de passe"/><br><br>
								<input type="text" name="ndf" placeholder="Nom"/><br><br>
								<input type="text" name="prenom" placeholder="Prénom"/><br><br>
								</div>
								
								<div class="div_colonne" type="droite">
								<input type="text" name="email" placeholder="Adresse E-mail"/><br><br>
								<div class="radio_genre">
										<label for="Homme">Homme</label>
										<input type="radio" id="Homme" name="Genre" value="Homme" checked/><br><br>
										<label for="Femme">Femme</label>
										<input type="radio" id="Femme" name="Genre" value="Femme" /><br><br>
										<label for="Autre">Autre</label>
										<input type="radio" id="Autre" name="Genre" value="Autre" /><br><br>
								</div>
										<input type="text" name="Birthday" placeholder="Jour de naissance"/><br><br>
										<input type="text" name="tel" placeholder="Numero de téléphone"/><br><br>
								</div>
									<input type="submit" name="envoi" />
							</fieldset>';
						exit();

					} else if ($erreur == 'usernamepris') {

						echo '<fieldset class="formulaire">
								<legend>Créez votre compte</legend>
								<div class="div_colonne" type="gauche">
								<input type="password" name="pwd" placeholder="Mot de passe"/><br><br>
								<input type="password" name="repeatpwd" placeholder="Répetez votre mot de passe"/><br><br>
								<input type="text" name="ndf" placeholder="Nom"/><br><br>
								<input type="text" name="prenom" placeholder="Prénom"/><br><br>
								</div>
								
								<div class="div_colonne" type="droite">
								<input type="text" name="email" placeholder="Adresse E-mail"/><br><br>
								<div class="radio_genre">
										<label for="Homme">Homme</label>
										<input type="radio" id="Homme" name="Genre" value="Homme" checked/><br><br>
										<label for="Femme">Femme</label>
										<input type="radio" id="Femme" name="Genre" value="Femme" /><br><br>
										<label for="Autre">Autre</label>
										<input type="radio" id="Autre" name="Genre" value="Autre" /><br><br>
								</div>
										<input type="text" name="Birthday" placeholder="Jour de naissance"/><br><br>
										<input type="text" name="tel" placeholder="Numero de téléphone"/><br><br>
								</div>
									<input type="submit" name="envoi" />
							</fieldset>';

						echo "Ce nom d'utilisateur est déjà pris! ";
						exit();

					} else if ($erreur == 'invalidusername&mail') {

						echo '<fieldset class="formulaire">
								<legend>Créez votre compte</legend>
								<div class="div_colonne" type="gauche">
								<input type="password" name="pwd" placeholder="Mot de passe"/><br><br>
								<input type="password" name="repeatpwd" placeholder="Répetez votre mot de passe"/><br><br>
								<input type="text" name="ndf" placeholder="Nom"/><br><br>
								<input type="text" name="prenom" placeholder="Prénom"/><br><br>
								</div>
								
								<div class="div_colonne" type="droite">
								<input type="text" name="email" placeholder="Adresse E-mail"/><br><br>
								<div class="radio_genre">
										<label for="Homme">Homme</label>
										<input type="radio" id="Homme" name="Genre" value="Homme" checked/><br><br>
										<label for="Femme">Femme</label>
										<input type="radio" id="Femme" name="Genre" value="Femme" /><br><br>
										<label for="Autre">Autre</label>
										<input type="radio" id="Autre" name="Genre" value="Autre" /><br><br>
								</div>
										<input type="text" name="Birthday" placeholder="Jour de naissance"/><br><br>
										<input type="text" name="tel" placeholder="Numero de téléphone"/><br><br>
								</div>
									<input type="submit" name="envoi" />
							</fieldset>';
						echo "Le format de l'email et le nom d'utilisateur ne sont pas valides.";
						exit();

					} else if ($erreur == 'pwdnotmatching') {

						echo '<fieldset class="formulaire">
								<legend>Créez votre compte</legend>
								<div class="div_colonne" type="gauche">
								<input type="password" name="pwd" placeholder="Mot de passe"/><br><br>
								<input type="password" name="repeatpwd" placeholder="Répetez votre mot de passe"/><br><br>
								<input type="text" name="ndf" placeholder="Nom"/><br><br>
								<input type="text" name="prenom" placeholder="Prénom"/><br><br>
								</div>
								
								<div class="div_colonne" type="droite">
								<input type="text" name="email" placeholder="Adresse E-mail"/><br><br>
								<div class="radio_genre">
										<label for="Homme">Homme</label>
										<input type="radio" id="Homme" name="Genre" value="Homme" checked/><br><br>
										<label for="Femme">Femme</label>
										<input type="radio" id="Femme" name="Genre" value="Femme" /><br><br>
										<label for="Autre">Autre</label>
										<input type="radio" id="Autre" name="Genre" value="Autre" /><br><br>
								</div>
										<input type="text" name="Birthday" placeholder="Jour de naissance"/><br><br>
										<input type="text" name="tel" placeholder="Numero de téléphone"/><br><br>
								</div>
									<input type="submit" name="envoi" />
							</fieldset>';

						echo "Les deux mots de passes ne correspondent pas.";
						exit();

					} else if ($erreur == 'DBerror') {

						echo "Erreur dans la base de données. Veuillez rafraîchir la page.";
						exit();
					} 



				} 

				
			 ?>
			</fieldset>
		</form>


	</body>
</html>
