<?php
	include_once 'includes/dbh-conn.php';
	include_once 'header.php';
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Louer un appartement</title>
</head>
<body>
    <h1>Faites votre demande de location auprès d'un propriétaire.</h1>
    <p> Sur cette page, remplissez le formulaire et votre demande sera envoyé au propriétaire de l'appartement demandé. Il pourra ensuite accepter votre requete.</p>

    <form action="demande-appartement.php" method="POST">

        <label for="nom_proprio">Nom de famille du propriétaire de votre appartement
            <input type="text" name="nom_proprio" id="nom_proprio"></br>
        </label></br>

        <label for="prenom_proprio">Prénom du propriétaire de votre appartement
            <input type="text" name="prenom_proprio" id="prenom_proprio" placeholder="Prenom proprio"></br>
        </label></br>

        <label for="type_appartement">Je souhaite louer le ...
            <select id="type_appartement" name="type_appartement">
                    <option value="Studio">Studio</option>
                    <option value="T1">T1</option>
                    <option value="T2">T2</option>
                    <option value="T3">T3</option>
                    <option value="T3 T4">T3 T4</option>
                    <option value="T5">T5</option>
                    <option value="T6">T6</option>
                    <option value="Duplex">Duplex</option>
                    <option value="Triplex">Triplex</option>
                    <option value="Souplex">Souplex</option>
                    <option value="Loft">Loft</option>
                </select>
        </label></br>

        <label for="numero_appart">Numéro ...
            <input type="number" name="numero_appart" id="numero_appart" placeholder="Numéro d'appartement"></br>
        </label>

        <p>Situé au </p>
        <label for="numero_maison">
            <input type="text" name="numero_maison" id="numero_maison" placeholder="Numéro de rue"></br>
        </label>
        <label for="Rue">
            <input type="text" name="Rue" id="Rue" placeholder="Rue"></br>
        </label> à 
        <label for="Ville_maison">
            <input type="text" name="Ville_maison" id="Ville_maison" placeholder="Ville"></br>
        </label></br>

        <label for="date_location_debut">Je suis locataire depuis le ...
            <input type="text" name="date_location_debut" id="date_location_debut" placeholder="Date de début de location"></br>
        </label>

        <input type="hidden" name="uId" id="uId" value="<?php echo $_SESSION['uId'] ?>" />
        
        <button type="submit" name="submit"> ajouter </button>

    </form>

    <?php 

    if (isset($_GET['erreur'])) {

        if ($_GET['erreur'] == "MaisonInexistante"){
            echo "La maison / appartement / le priopriétaire demandé n'est pas correcte.";
        }


    }
    
    
    
    ?>





</body>
</html>