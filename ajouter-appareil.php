<?php
	include_once 'header.php';
	include_once 'includes/dbh-conn.php';



if (isset( $_GET['IdPiece'] ) && isset( $_SESSION['uId'] ) ) {


    //==========================================================================//

    // Trouver la maison d'origine

    $sql = "SELECT * 
			FROM proprietaire P
			WHERE (P.IdMaison = (SELECT A.IdMaison
                                FROM appartement A
                                WHERE A.IdAprt = (SELECT piece.IdAprt
                                                  FROM piece 
                                                  WHERE (piece.IdPiece = ?)
                                                  )
                                )
                    ) 
                    AND (P.IdU = ?)";

	$stmt = mysqli_stmt_init($conn);
	mysqli_stmt_prepare($stmt, $sql);
	mysqli_stmt_bind_param($stmt, "ii", $_GET['IdPiece'] , $_SESSION['uId']);
	mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);
    $idMaison = mysqli_fetch_assoc($result)['IdMaison'];


    $test = "SELECT *
            FROM piece P
            WHERE (P.IdPiece = ?);";

    $test_stmt = mysqli_stmt_init($conn);
    mysqli_stmt_prepare($test_stmt, $test);
    mysqli_stmt_bind_param($test_stmt, "i", $_GET['IdPiece'] );
    mysqli_stmt_execute($test_stmt);

    $res = mysqli_stmt_get_result($test_stmt);
    $IdAppart = mysqli_fetch_assoc($res)['IdAprt'];

	echo "<a href='gerer-maison-perso.php'>Mes maisons</a>  
          <a href='gerer-maison-perso.php?IdMaison=".$idMaison."'> Mes appartements </a>
          <a href='MesPieces.php?&IdAppart=".$IdAppart."'> Mes pieces</a>" ;

    //==========================================================================//
}

else {
	ob_end_clean();
	header('HTTP/1.0 404 Not Found');
	exit();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>


    <?php
    

    //==========================================================================//
    
    // Recuperer tous les appareils
    echo "<h1>Mes appareils</h1>";

    $sql = "SELECT *
            FROM piece  
            WHERE IdPiece = ?;";

    mysqli_stmt_prepare($stmt, $sql);
    mysqli_stmt_bind_param($stmt, "i",  $_GET['IdPiece']);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);

    $colonne = mysqli_fetch_assoc($result);

    echo "<h3>Dans \" ".$colonne['NomPiece']."    \" </h3>";

    $sql = "SELECT *
            FROM appareil
            WHERE (IdPiece = ?);";

    
    $stmt = mysqli_stmt_init($conn);
    mysqli_stmt_prepare($stmt, $sql);
    mysqli_stmt_bind_param($stmt, "i",  $_GET['IdPiece']);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);

    //On loop au travers de chaque appareil trouvé

    while ($colonne = mysqli_fetch_assoc($result)){

        $a_allumer = "allumer_appareil_" . $colonne['IdApp'];
        $a_eteindre = "eteindre_appareil_" . $colonne['IdApp'];


        // On verifie si on vient de cliquer sur le boutton pour éteindre un appareil.
        // On peut faire ca avant les autres vérification, parce qu'on sait d'avance qu'il y a au moins un appareil dans la liste.
        if (isset($_POST[$a_eteindre])   ) {


            $index = array_search($colonne['IdApp'], $_SESSION['appareils_allumes'],FALSE);
            unset( $_SESSION['appareils_allumes'][$index]  ); // On supprime de la liste des appareils allumés l'appareil en question
            $_POST[$a_eteindre] = null;


            // Comme on va eteindre, on sait que la derniere fois que IdApp apparait dans la table consommation cest la bonne fois
            
            $sql = "UPDATE consommation
                    SET date_fin = NOW()
                    WHERE (IdApp = ?) AND (date_fin=0);";



            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt, $sql);
            mysqli_stmt_bind_param($stmt, "i", $colonne['IdApp']);

            if (!mysqli_stmt_execute($stmt)){
                
                echo 'query error : ' . mysqli_error($conn);
            }

            
        }
        

        // On verifie si l'appareil vient d'être allumé OU est déjà allumé.
        if (isset($_POST[$a_allumer]) ||  (isset($_SESSION['appareils_allumes']) && in_array($colonne['IdApp'], $_SESSION['appareils_allumes'])) ){

            // S'il vient d'être allumé, on l'ajoute à la liste des appareils allumés.            
            if (isset($_SESSION['appareils_allumes']) && !in_array($colonne['IdApp'], $_SESSION['appareils_allumes']) ){

                array_push($_SESSION['appareils_allumes'], $colonne['IdApp']); // On ajoute à notre tableau d'appareils allumé l'Id de l'appareil allumé.
            } 


            // Si l'appâreil est dans la liste des appareils allumés, on affiche un bouton eteindre
            if (in_array($colonne['IdApp'], $_SESSION['appareils_allumes'])){

                //On affiche un bouton eteindre
                echo "<p>Mon appareil : ".$colonne['NomApp']."</p> 

                    <form action='ajouter-appareil.php?IdPiece=".$_GET['IdPiece']."' method='POST'>

                    <button type='submit' name='eteindre_appareil_".$colonne['IdApp']."'> Eteindre </button>

                    </form>";
            }
 
        }

        // Que pour celui qu'on vient d'allumer, on l'ajoute dans la table consommation
        if (in_array($colonne['IdApp'], $_SESSION['appareils_allumes']) && isset($_POST[$a_allumer]))  {

            $sql = "INSERT INTO consommation(IdApp, date_debut)
                    VALUES(?,NOW());";


            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt, $sql);
            mysqli_stmt_bind_param($stmt, "i",$colonne['IdApp']);


            if (!mysqli_stmt_execute($stmt)){

                echo 'query error : ' . mysqli_error($conn);
            }

        }

        
        // Sinon, on affiche un bouton "allumer".
        else if (!in_array($colonne['IdApp'], $_SESSION['appareils_allumes'])){
            // Ce formulaire redirige vers la meme page, mais envoie en POST la variable "allumer_appareil_[IdApp]" en cliquant sur le boutton "allumer".
            // Du grand génie. 
            echo "<p>Mon appareil : ".$colonne['NomApp']."</p> 

                <form action='ajouter-appareil.php?IdPiece=".$_GET['IdPiece']."' method='POST'>

                <button type='allumer_appareil_".$colonne['IdApp']."' name='allumer_appareil_".$colonne['IdApp']."'> Allumer </button> 

                </form>";
        }


        // On affiche les bouttons relatif à chaque appareil (supprimer)

        echo "        
        <form action='supprimer-element.php?element=appareil&IdAppareil=".$colonne['IdApp']."' method='POST'>
        
            <button type='submit' name='delete_button'>Supprimer cet appareil</button>

        </form>     
        ";


        

    }
    
    ?>

    <br><br>
    <h3>J'ajoute un appareil </h3>

    <form action="addappareil.php?IdPiece=<?php echo $_GET['IdPiece']?>" method="POST" class="ajouter_appareil_form">


        <label> C'est quoi ? :
            <input type="text" name="NomApp" id="NomApp" placeholder="NomApp">
        </label>
        <br><br>

        <label for='type_appareil'> Quel est cet appareil ? 
        <select id="type_appareil" name="type_appareil">
        <?php

        
        
        $sql = "SELECT * 
			    FROM typeappareil
			    ORDER BY LibelleTypeAppareil;";

        $stmt = mysqli_stmt_init($conn);
        mysqli_stmt_prepare($stmt, $sql);
        mysqli_stmt_execute($stmt);

        $result = mysqli_stmt_get_result($stmt);

		while ($colonne = mysqli_fetch_assoc($result)){

            print_r($colonne);

            echo "<option name='TypeApp' id='TypeApp' value='".$colonne['IdTypeAppareil']."'>".$colonne['LibelleTypeAppareil']."
                  </option>";

        } ?>

        </select><br><br>
        </label>


        <label> Position :
            <input type="text" name="Pos" id="Pos" placeholder="Pos.">
        </label>

        <label> Consommation par heure :
            <input type="text" name="conso_par_heure" id="conso_par_heure" placeholder="consommation">
        </label>

        <label> Emission par heure :
            <input type="text" name="emission_par_heure" id="emission_par_heure" placeholder="emission">
        </label>


        <br><br>
        <button type="submit" name="submit"> ajouter </button>

    </form>
</body>
</html>