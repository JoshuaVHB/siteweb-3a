<?php
include_once 'includes/dbh-conn.php';


if (isset($_POST['submit'])){

    // Read data
    $type_piece = $_POST['type_piece'];
    $nom_piece = $_POST['Nom_piece'];

    // Escape potential harmful data
    $type_piece = mysqli_real_escape_string($conn, $type_piece);
    $idAppart = mysqli_real_escape_string($conn, $_GET['IdAppart'] );
    $nom_piece = mysqli_real_escape_string($conn, $nom_piece);
    

    // Inserer dans les tables 

    $sql = "INSERT INTO piece(NomPiece,IdAprt)
            VALUES(?,?);";



    $stmt = mysqli_stmt_init($conn);
    mysqli_stmt_prepare($stmt, $sql);
    mysqli_stmt_bind_param($stmt, "si", $nom_piece, $idAppart);

    if (mysqli_stmt_execute($stmt)){

        $sql2 =  "INSERT INTO appartenirpiece(IdPiece,IdTypePiece) VALUES ((SELECT MAX(IdPiece) FROM piece),?);";
        $stmt2 = mysqli_stmt_init($conn);
        mysqli_stmt_prepare($stmt2, $sql2);
        mysqli_stmt_bind_param($stmt2, "i", $type_piece);

        if (mysqli_stmt_execute($stmt2)){
            header('Location: MesPieces.php?IdAppart='.$idAppart.'&addpiece=succes');
            mysqli_stmt_close($stmt);
            mysqli_stmt_close($stmt2);
            mysqli_close($conn);
            exit();
        }

        else {

            header('Location: gerer-maison-perso.php?&addpiece=error');
            exit();
        }




        
    } else {

        echo "Failed to add app";
        echo 'query error : ' . mysqli_error($conn);

    }





}



?>