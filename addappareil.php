<?php
include_once 'includes/dbh-conn.php';


if (isset($_POST['submit'])){

    // Read data
    $NomApp = $_POST['NomApp'];
    $type_appareil = $_POST['type_appareil'];
    $Pos = $_POST['Pos'];

    $emission_par_heure = $_POST['emission_par_heure'];
    $conso_par_heure = $_POST['conso_par_heure'];


    // Escape potential harmful data
    $NomApp = mysqli_real_escape_string($conn, $NomApp);
    $type_appareil = mysqli_real_escape_string($conn, $type_appareil);
    $Pos = mysqli_real_escape_string($conn, $Pos);
    $IdPiece = mysqli_real_escape_string($conn, $_GET['IdPiece'] );
    $emission_par_heure = mysqli_real_escape_string($conn, $emission_par_heure );
    $conso_par_heure = mysqli_real_escape_string($conn, $conso_par_heure );

    //--------------------------//

    // Recuperer la substance emises

    $sql_get_app_substances = "SELECT * 
                        FROM substance S 
                        INNER JOIN emettre E ON (E.idSubstance = S.idSubstance)
                        INNER JOIN typeappareil T ON (T.IdTypeAppareil = E.IdTypeAppareil)
                        WHERE T.IdTypeAppareil=?;";

    $stmt_get_app_substances = mysqli_stmt_init($conn);
    mysqli_stmt_prepare($stmt_get_app_substances, $sql_get_app_substances);
    mysqli_stmt_bind_param($stmt_get_app_substances, 's', $type_appareil);
    mysqli_stmt_execute($stmt_get_app_substances);

    $res = mysqli_stmt_get_result($stmt_get_app_substances);

    
    // Recuperer la ressources necessaires
    
    $sql_get_app_ressources = "SELECT * 
                            FROM ressource R 
                            INNER JOIN utiliser U ON (R.IdRessource = U.IdRessource) 
                            INNER JOIN typeappareil T ON (T.IdTypeAppareil = U.IdTypeAppareil)
                            WHERE T.IdTypeAppareil=?;";

    $stmt_get_app_ressources = mysqli_stmt_init($conn);
    mysqli_stmt_prepare($stmt_get_app_ressources, $sql_get_app_ressources);
    mysqli_stmt_bind_param($stmt_get_app_ressources, 's', $type_appareil);
    mysqli_stmt_execute($stmt_get_app_ressources);

    $res2 = mysqli_stmt_get_result($stmt_get_app_ressources);

    //--------------------------//

    mysqli_stmt_close($stmt_get_app_substances);
    mysqli_stmt_close($stmt_get_app_ressources);

    // create sql
    $sql = "INSERT INTO appareil(NomApp,PositionApp,TypeApp,IdPiece)
            VALUES(?,?,?,?);";

    $stmt = mysqli_stmt_init($conn);
    mysqli_stmt_prepare($stmt, $sql);
    mysqli_stmt_bind_param($stmt, "ssii", $NomApp, $Pos, $type_appareil, $IdPiece);

    if (mysqli_stmt_execute($stmt)){



        // Ajouter dans les tables emettreappareil et conso appareil

        while ($ressource = mysqli_fetch_array($res2)){

            $sql2 = "INSERT INTO consoappareil(IdApp,IdRessource,consomme_par_heure)
                    VALUES((SELECT MAX(IdApp) FROM appareil),?,?);";
    
            $stmt2 = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt2, $sql2);
            mysqli_stmt_bind_param($stmt2, "ii", $ressource, $conso_par_heure);

            mysqli_stmt_execute($stmt2);

        }


        $substance = mysqli_fetch_array($res);

        while ($substance = mysqli_fetch_array($res)) {

            echo $substance;
            $sql3 = "INSERT INTO emetappareil(IdApp,idSubstance,	emission_par_heure)
                    VALUES((SELECT MAX(IdApp) FROM appareil),?,?);";
    
            $stmt3 = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt3, $sql3);
            mysqli_stmt_bind_param($stmt3, "ii", $substance['idSubstance'], $emission_par_heure);

            mysqli_stmt_execute($stmt3);

        }

        header('Location: ajouter-appareil.php?IdPiece='.$IdPiece.'');

        mysqli_stmt_close($stmt);
		mysqli_close($conn);

        exit();

        
    } else {

        echo "Failed to add app";
        echo 'query error : ' . mysqli_error($conn);

    }




}



?>