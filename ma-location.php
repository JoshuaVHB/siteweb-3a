<?php
	include_once 'includes/dbh-conn.php';
	include_once 'header.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <?php 

    $check_location = "SELECT *
                        FROM `location`
                        WHERE IdU = ?";

    $stmt = mysqli_stmt_init($conn);
    mysqli_stmt_prepare($stmt, $check_location);
    mysqli_stmt_bind_param($stmt, "i", $_SESSION['uId']);
    mysqli_stmt_execute($stmt);

    $res = mysqli_stmt_get_result($stmt);

    $colonne = mysqli_fetch_assoc($res);


    if (isset($_GET['IdAprt']) && $colonne ){
        
        $IdAprt = $_GET['IdAprt'];
        echo "<h1>Ma location</h1></br>
            <p>Sur cette page, vous pouvez retrouver les informations concernant votre location.</br>
             Vous pouvez également consulter votre consommation personelle sur la page \"Ma consommation\" disponible sur votre profil.
             </br>Ici, vous trouverez la consommation de votre logement.</p>";

    
        $link_quitter = "ma-location.php?QuitterLocation=" . $IdAprt ;
        
        echo   "<a href='".$link_quitter."'> Quitter ma location </a>";
    }

    if (isset($_GET['QuitterLocation'])){

        $IdAprt = $_GET['QuitterLocation'];
        // $IdAprt = mysqli_real_escape_string($conn,$IdAprt);

        $sql = " UPDATE `location`
                SET DateFin	= NOW()
                WHERE IdAprt = ? AND DateFin IS NULL;";

        $stmt = mysqli_stmt_init($conn);
        mysqli_stmt_prepare($stmt, $sql);
        mysqli_stmt_bind_param($stmt, "i", $IdAprt);



        

    }

    ?>
    
</body>
</html>