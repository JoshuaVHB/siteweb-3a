<?php
	include_once 'includes/dbh-conn.php';
	include_once 'header.php';


if (isset($_SESSION['uId'])){


    $sql = "    SELECT U.IdU, U.Nom, U.Prenom, M.Numero, M.Rue, M.Ville,
                       A.NumeroApprt, T.LibelleTypeApp, U.Genre, D.IdDemande, D.IdAprt
                FROM demande_location D
                INNER JOIN appartement A ON (D.IdAprt = A.IdAprt)
                INNER JOIN maison M ON (A.IdMaison = M.IdMaison)
                INNER JOIN utilisateur U ON (D.IdLocataire = U.IdU)
                INNER JOIN typeapp T ON (A.IdTypeAppartement = T.IdTypeAppartement)
                WHERE D.IdProprio = ?";

    $stmt = mysqli_stmt_init($conn);
    mysqli_stmt_prepare($stmt, $sql);
    mysqli_stmt_bind_param($stmt, "i", $_SESSION['uId']);
    mysqli_stmt_execute($stmt);

    $res = mysqli_stmt_get_result($stmt);

    while ($colonne = mysqli_fetch_assoc($res)){

        // On verifie qu'on ne vient pas de cliquer sur accepter la demande
        if (isset($_GET['AccDemande']) && $_GET['AccDemande']==$colonne['IdDemande'] ){


            echo "On vient de cliquer sur accepter cette demande ! ";

            $sql2 = "INSERT INTO  `location`(IdAprt, DateDeb, IdU)
                     VALUES (?,NOW(),?);";

            $stmt2 = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt2, $sql2);
            mysqli_stmt_bind_param($stmt2, "ii", $colonne['IdAprt'], $colonne['IdU']);
            if (mysqli_stmt_execute($stmt2)) {

                echo $colonne['IdDemande'];

                $sql3 = "DELETE FROM `demande_location`
                        WHERE (IdDemande = ?);";

                $stmt3 = mysqli_stmt_init($conn);
                mysqli_stmt_prepare($stmt3, $sql3);
                mysqli_stmt_bind_param($stmt3, "i", $colonne['IdDemande'] );

                if (mysqli_stmt_execute($stmt3)){

                    header('Location: ./mes-demandes.php');
                }

            }

        } else {

            echo "Demande de ".$colonne['Nom']." ".$colonne['Prenom']."</br>
              Pour le ".$colonne['LibelleTypeApp']." numéro ".$colonne['NumeroApprt']."
               situé au ".$colonne['Numero']." rue '".$colonne['Rue']."' à ".$colonne['Ville']."";

            echo "
           
            </br><a href='mes-demandes.php?AccDemande=".$colonne['IdDemande']."' >Accepter la demande </a> </br></br>
                <a href='mes-demandes.php?RefDemande=".$colonne['IdDemande']."' >Refuser la demande </a>";


        }











    }

    
    
}






else {
	header('Location: ../inscription.php');
	exit();
}



?>
