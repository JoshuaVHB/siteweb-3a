<?php
	include_once 'includes/dbh-conn.php';
	session_start();
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" lang="fr">
		<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
		<link href="stylesheets/styleHEADER2.css" rel="stylesheet">
		<link rel="icon" href="logo.png" />

	</head>

	<body>

			<header>

			<?php 
			
			if (isset($_SESSION['Admin']) &&  $_SESSION['Admin'] == 1){

				echo "<span class='admin_mode'>Vous etes en mode administrateur.</br></span>";
			}

			// On va vérifier si des utilisateurs nous ont envoyé une demande de location

			if (isset($_SESSION['uId'])){

				$sql = " SELECT count(*) as nombre_demandes 
						FROM demande_location
						WHERE IdProprio = ?;";


				$stmt = mysqli_stmt_init($conn);
				mysqli_stmt_prepare($stmt, $sql);
				mysqli_stmt_bind_param($stmt, "i", $_SESSION['uId']);
				mysqli_stmt_execute($stmt);

				$res = mysqli_stmt_get_result($stmt);

				// On trouve au moins un résultat :
				if ($value = mysqli_fetch_assoc($res)['nombre_demandes']!=0) {


					echo "<a class='demandes_css' href='mes-demandes.php'>Vous avez ".$value." demande(s) de location !</a>";

				}
			}
			
			?>

			<div class="header-tete">
				<a href="index.php" class="logo" ><img src="logo.png"/></a>
				<a href="index.php" class="marque">Maison</a>
			</div>


			<nav class="menu">

				<ul>

					<?php 


					if (empty($_SESSION['uId'])) {

						echo "<li><a href='login.php' class='inscription' >Se Connecter / S'inscrire</a></li>";

					} else {

						echo "	
								<li><a class='inscription' href='profil.php'> Mon profil </a></li>

								<form action='includes/logout.php' method='post' class='logout-box'>

										<input type='submit' name='logout-submit' value='Se déconnecter'/>

							</form>";
						


					
					} ?>
				</ul>

			</nav>

			</header>

