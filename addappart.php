<?php
include_once 'includes/dbh-conn.php';


if (isset($_POST['submit'])){

    // Read data
    $nb_hab = $_POST['nb_hab'];
    $degre_secu = $_POST['degre_secu'];
    $type_app = $_POST['type_app'];
    $numero_appart = $_POST['numero_appart'];


    // Escape potential harmful data
    $type_app = mysqli_real_escape_string($conn, $type_app);
    $nb_hab = mysqli_real_escape_string($conn, $nb_hab);
    $degre_secu = mysqli_real_escape_string($conn, $degre_secu);
    $numero_appart = mysqli_real_escape_string($conn, $numero_appart);
    $idMaison = mysqli_real_escape_string($conn, $_GET['IdMaison'] );


    // Recuperer le type appart


    $get_app_id = "SELECT *
                    FROM typeapp
                    WHERE LibelleTypeApp = ? ;";

    $stmt_get_id_app = mysqli_stmt_init($conn);
    mysqli_stmt_prepare($stmt_get_id_app, $get_app_id);
    mysqli_stmt_bind_param($stmt_get_id_app, 's', $type_app);
    mysqli_stmt_execute($stmt_get_id_app);

    $res = mysqli_stmt_get_result($stmt_get_id_app);

    $type_appartement = mysqli_fetch_assoc($res)['IdTypeAppartement'];



    mysqli_stmt_close($stmt_get_id_app);

    // create sql
    $sql = "INSERT INTO appartement(DegredeSecu,NombreHab,NumeroApprt,IdTypeAppartement,IdMaison)
            VALUES(?,?,?,?,?);";

    $stmt = mysqli_stmt_init($conn);
    mysqli_stmt_prepare($stmt, $sql);
    mysqli_stmt_bind_param($stmt, "siiii", $degre_secu, $nb_hab,$numero_appart , $type_appartement, $idMaison);

    if (mysqli_stmt_execute($stmt)){
        
        header('Location: gerer-maison-perso.php?IdMaison='.$idMaison.'&addappart=succes');

        mysqli_stmt_close($stmt);
		mysqli_close($conn);

        exit();

        
    } else {

        echo "Failed to add app";
        echo 'query error : ' . mysqli_error($conn);

    }




}



?>