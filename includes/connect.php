<?php

if (isset($_POST['Connexion'])) {

	require 'dbh-conn.php';


	$username = $_POST['mailId'];
	$password = $_POST['password'];


	$username = mysqli_real_escape_string($conn, $username);
	$password = mysqli_real_escape_string($conn, $password);


	if (empty($username) || empty($password)) {
		header('Location: ../login.php?erreur=champsvides');
		exit();
	}

	else {



		$sql = "SELECT * FROM utilisateur WHERE Nom=? OR Email=?;";

		$stmt = mysqli_stmt_init($conn);
		mysqli_stmt_prepare($stmt, $sql);
		mysqli_stmt_bind_param($stmt,'ss',$username, $username);

		if (mysqli_stmt_execute($stmt)) {

			$Resultat = mysqli_stmt_get_result($stmt);

			$ligne = mysqli_fetch_assoc($Resultat);

			if (!$ligne){ 
				header('Location: ../login.php?erreur=existepas');
			}


			if ($ligne) {


				$hashedPwd = $ligne['MDP'];

				$pwdCheck = password_verify($password, $hashedPwd);


				if ($pwdCheck === false) {
					header('Location: ../login.php?erreur=wrongpwd&password='.$password.'&hashedPsd='.$hashedPwd);
				}

	

				else if ($pwdCheck === true) {

					// On démarre la session et tout


					session_start();

					$_SESSION['userMail'] = $ligne['Email'];
					$_SESSION['uId'] = $ligne['IdU'];
					$_SESSION['Prenom'] = $ligne['Prenom'];
					$_SESSION['appareils_allumes'] = array(); // Utilisé pour suivre les appareils allumés au cours de la session, entre les différents page

					// :On récupère la valeur de admin pour passer en mode admin....
					$sql2 = "SELECT * FROM utilisateur WHERE IdU = ?";
					$stmt2 = mysqli_stmt_init($conn);

					mysqli_stmt_prepare($stmt2, $sql2);
					mysqli_stmt_bind_param($stmt2,'i', $ligne['IdU']);

					mysqli_stmt_execute($stmt2);
					$res = mysqli_stmt_get_result($stmt2);
					$value = mysqli_fetch_assoc($res)['Admin'];

					if (!$res){
						header('Location: ../index.php?signup=bof&admin='.$_SESSION['Admin'].'&value='.$value.'&Idu='. $ligne['IdU'].'');
						exit();
					}


					$_SESSION['Admin'] = $value;


					header('Location: ../index.php?signup=success');
					exit();

				}
			}
		}
	}

}


else {
	header('Location: ../inscription.php');
	exit();
}