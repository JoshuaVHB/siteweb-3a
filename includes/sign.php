<?php

if (isset($_POST['envoi'])) {


	require 'dbh-conn.php';

	$email = $_POST['email'];
	$pwd = $_POST['pwd'];
	$rpwd = $_POST['repeatpwd'];
	$prenom = $_POST['prenom'];
	$ndf = $_POST['ndf'];
	$tel = $_POST['tel'];
	$NumMaison = $_POST['NumMaison'];
	$Rue = $_POST['Rue'];
	$CP = $_POST['CP'];
	$Ville = $_POST['Ville'];
	$Birthday = ($_POST['Birthday']);
	$genre = $_POST['Genre'];
	$new_date = date('Y-m-d', strtotime($Birthday));

	 if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		header('Location: ../inscription.php?erreur=invalidmail');
		exit();

	} else if (!preg_match("/^[0-9]*$/", $tel) && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
		header('Location: ../inscription.php?erreur=invalidTel');
		exit();

	} else if (!preg_match("/^[0-9]*$/", $tel) && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
		header('Location: ../inscription.php?erreur=invalidCp');
		exit();

	} 
	else if (($pwd !== $rpwd)) {
		header('Location: ../inscription.php?erreur=pwdnotmatching&mail='.$email.'&username='.$username);
		exit();
	}
	else if (time() < strtotime('+18 years',  strtotime($Birthday))) {
		echo 'Client is under 18 years of age.';
		header('Location: ../inscription.php?erreur=Under18');
		exit;
	}
	else {

		$hashPwd = password_hash($pwd, PASSWORD_DEFAULT);

		$adresse = $NumMaison . ' ' . $Rue . ' ' . $Ville . ' '. $CP;


		$sql = "INSERT INTO utilisateur (MDP, Email, Nom, Prenom, 
										Genre, DateNaissance, Tel,
										Actif, DateCrea, Admin)

				VALUES (?,?,?,?,?,?,?, 1 , NOW(), 0 );";

		$stmt = mysqli_stmt_init($conn);
		mysqli_stmt_prepare($stmt, $sql);
		mysqli_stmt_bind_param($stmt, "sssssss", $hashPwd,$email,$ndf,$prenom,$genre,$new_date,$tel);
		if (!mysqli_stmt_execute($stmt)) {
			echo 'query error : ' . mysqli_error($conn);
			header('Location: ../inscription.php?erreur=DBerror');
			
			exit();

		} else {

			$checkResultat = mysqli_stmt_num_rows($stmt);

			if ($checkResultat > 0) {
				header('Location: ../inscription.php?erreur=usernamepris&email='.$email.'');
				mysqli_stmt_close($stmt);
				mysqli_close($conn);
				exit();

			} else {

				header('Location: ../login.php?inscription=succes');
				echo "Inscription réussie !";
				mysqli_stmt_close($stmt);
				mysqli_close($conn);
				exit();
				}
		}
			
	}
}

else {
	header('Location: ../inscription.php');
	exit();
}