<?php
include_once 'includes/dbh-conn.php';
include_once 'header.php';

if (isset($_POST['submit'])){

    // Read data
    $Nom = $_POST['Nom'];
    $Eval = $_POST['eval_eco'];
    $Rue = $_POST['Rue'];
    $CP = $_POST['CP'];
    $Numero = $_POST['Numero'];
    $Ville = $_POST['Ville'];
    

    // Escape potential harmful data
    $Nom = mysqli_real_escape_string($conn, $Nom);
    $Eval = mysqli_real_escape_string($conn, $Eval);
    $Rue = mysqli_real_escape_string($conn, $Rue);
    $CP = mysqli_real_escape_string($conn, $CP);
    $Numero = mysqli_real_escape_string($conn, $Numero);
    $Ville = mysqli_real_escape_string($conn, $Ville);


    // create sql
    $sql = "INSERT INTO maison(Rue,Numero,Eval,nom,Ville,CP,IdVille)
            VALUES(?,?,?,?,?,?,(SELECT IdVille
                                FROM ville
                                WHERE NomVille = ? OR CodePostal = ?));";
                                                                    
                               
    $stmt = mysqli_stmt_init($conn);
    mysqli_stmt_prepare($stmt, $sql);
    mysqli_stmt_bind_param($stmt, "sisssisi",$Rue,$Numero,$Eval,$Nom,$Ville,$CP,$Ville,$CP);
                                                                    
    if (mysqli_stmt_execute($stmt)){

        echo ($_SESSION['uId']);
        
        $sql2 =  "INSERT INTO proprietaire(IdMaison,DateDebP,IdU) VALUES ((SELECT MAX(IdMaison) FROM maison) 
                                                                        ,NOW()
                                                                        ,?);";

        $stmt2 = mysqli_stmt_init($conn);
        mysqli_stmt_prepare($stmt2, $sql2);
        mysqli_stmt_bind_param($stmt2, "i", $_SESSION['uId']);

        //save to db
        if (  mysqli_stmt_execute($stmt2) ){
            // success
            header('Location: profil.php');
        } else {
            header('Location: ajouter-maison.php');
            exit();
        }
        
    } else {
        header('Location: ajouter-maison.php');
        exit();

    }




}



?>